﻿import arcpy

buildable_polygon = arcpy.GetParameterAsText(0)
project_name = arcpy.GetParameterAsText(1)
project_id = arcpy.GetParameterAsText(2)
buildable_version = arcpy.GetParameterAsText(3)
buildable_acres = arcpy.GetParameterAsText(4)

wind_or_solar = arcpy.GetParameterAsText(5)
update_project_history = arcpy.GetParameter(6)

boundary_used = arcpy.GetParameterAsText(7)
post_proc = arcpy.GetParameterAsText(8)
comments = arcpy.GetParameterAsText(9)
technology = arcpy.GetParameterAsText(10)
ex_use = arcpy.GetParameterAsText(11)

# debugging
# buildable_polygon = r'G:\Projects\USA_East\Bradley_Solar\05_GIS\053_Data\working\BL_v9_Bradley_Solar_20231213.gdb\Buildable_Land\BL_v9_Bradley_Solar_20231213'
# project_name = 'Bradley Solar'
# project_id = '3108'
# buildable_version = '9'
# buildable_acres = '1526.16'
#
# wind_or_solar = 'Solar/Other'
# update_project_history = True
#
# boundary_used = r'G:\Projects\USA_East\Bradley_Solar\05_GIS\053_Data\Bradley_Solar.gdb\Boundaryv3_20231213TEST'
# post_proc = None
# comments = 'Updating Boundary.'
# technology = 'Solar'
# ex_use = None

if wind_or_solar == 'Solar/Other':
    buildables_feature = "https://geoportal.edf-re.com/rag/rest/services/Other/Solar_Buildable_Lands/FeatureServer/14"
    buildables_feature = r"G:\General\GIS\SDEConnections\SDHQRAGDBPRD01_webmap.sde\DBO.Buildable_Lands_Solar_WGS84"
elif wind_or_solar == 'Wind':
    buildables_feature = "https://geoportal.edf-re.com/rag/rest/services/Other/Wind_Buildable_Lands/FeatureServer/24"
    buildables_feature = r"G:\General\GIS\SDEConnections\SDHQRAGDBPRD01_webmap.sde\DBO.Buildable_Lands_Wind_WGS84"

project_history_fs = r"https://geoportal.edf-re.com/rag/rest/services/Other/Project_History/FeatureServer/2"

sde_fields = ['ProjectName', 'Project_ID', 'Buildable_Version', 'Notes', 'Area_ac', 'Coordinate_System', 'SHAPE@']

out_table = r"G:\Users\Ardy\tmp\CheckBuildablesGeomResult.txt"

# Set workspace environment to geodatabase
arcpy.env.workspace = r"G:\General\GIS\SDEConnections\SDHQRAGDBPRD01_webmap.sde"

try:
    arcpy.MultipartToSinglepart_management(buildable_polygon, r"in_memory\Exploded")
    arcpy.RepairGeometry_management(r"in_memory\Exploded", validation_method='OGC')

    num_features = arcpy.GetCount_management(r"in_memory\Exploded").getOutput(0)

    if len(num_features) > 1:
        arcpy.Dissolve_management(r"in_memory\Exploded", r"in_memory\Dissolved", "", "", "MULTI_PART")
        arcpy.RepairGeometry_management(r"in_memory\Dissolved", validation_method='OGC')
    else:
        arcpy.Dissolve_management(r"in_memory\Exploded", r"in_memory\Dissolved", "", "", "MULTI_PART")
        arcpy.RepairGeometry_management(r"in_memory\Dissolved", validation_method='OGC')

    arcpy.Buffer_analysis(r"in_memory\Dissolved", r"in_memory\Buffer", "1 Feet", "FULL")
    arcpy.RepairGeometry_management(r"in_memory\Buffer", validation_method='OGC')

    arcpy.AddMessage("Writing buildable to SDE")
    deleteWhere = "ProjectName = '{}' AND Project_ID = {} AND buildable_version = {}".format(project_name, project_id,
                                                                                             buildable_version)
    # per JL - update with a delete and append instead of using cursors
    with arcpy.da.UpdateCursor(buildables_feature, ["ProjectName", "Project_ID", "buildable_version"],
                               where_clause=deleteWhere) as uCur:
        for row in uCur:
            arcpy.AddMessage(f"Found & deleting duplicate row -- {row}")
            uCur.deleteRow()

    for row in arcpy.da.SearchCursor(r"in_memory\Buffer", "SHAPE@"):
        shape = row

    spatial_ref = arcpy.Describe(r"in_memory\Buffer").spatialReference
    factory_code = str(spatial_ref.factoryCode)

    with arcpy.da.InsertCursor(buildables_feature, sde_fields) as cursor:
        # Set values for fields
        row_values = (project_name, project_id, buildable_version,
                      comments, buildable_acres, factory_code,
                      shape[0])

        # Insert the new feature into the SDE layer
        cursor.insertRow(row_values)

    arcpy.AddMessage("Finished writing Buildable to table.")

    if update_project_history:
        arcpy.AddMessage(f"Updating Project History for {project_name}, {project_id}, {buildable_version}")

        # Find Turbine Manufacturer and Model to append into project history table
        if wind_or_solar == 'Wind':
            wind_search_where = "ProjectName = '{}' AND Project_ID = {} AND Buildable_Version = {}".format(project_name,
                                                                                                     project_id,
                                                                                                     buildable_version)
            with arcpy.da.SearchCursor('https://geoportal.edf-re.com/rag/rest/services/Other/Wind_Constraint_Tracker/FeatureServer/4', ['Turbine_Manufacturer', 'Turbine_Alias'], where_clause = wind_search_where) as windDB_info_cursor:
                for turbine_data_row in windDB_info_cursor:
                    if turbine_data_row[0] is not None and turbine_data_row[1] is not None:
                        turbine_manufacturer = turbine_data_row[0]
                        turbine_model = turbine_data_row[1]
                    else:
                        turbine_manufacturer = None
                        turbine_model = None

            # append to the history table using field mapping to make sure the fields are correct
            fields = ['projectname',
                      'Project_ID',
                      'Buildable_Version',
                      'Comments',
                      'Post_Processing',
                      'Technology',
                      'External_Use',
                      'Boundary_Used',
                      'Buildable_Acres',
                      'Boundary_Acres',
                      'Turbine_Manufacturer',
                      'Turbine_Model']

            boundary_desc = arcpy.Describe(boundary_used)
            boundary_used = boundary_desc.catalogPath

            geometries = arcpy.CopyFeatures_management(boundary_used, arcpy.Geometry())
            boundary_acres = geometries[0].getArea("GEODESIC", "ACRES")

            # Execute MakeTableView and delete existing records & update
            deleteWhere = "ProjectName = '{}' AND Project_ID = {} AND buildable_version = {}".format(project_name,
                                                                                                     project_id,
                                                                                                     buildable_version)

            with arcpy.da.UpdateCursor(project_history_fs, ["ProjectName", "Project_ID", "buildable_version"],
                                       where_clause=deleteWhere) as uCur:
                for row in uCur:
                    arcpy.AddMessage(f"Found & deleting duplicate row -- {row}")
                    uCur.deleteRow()

            arcpy.AddMessage(f"Acres for boundary is - {boundary_acres}")

            cursor = arcpy.da.InsertCursor(project_history_fs,
                                           fields)

            cursor.insertRow(
                (project_name, project_id, buildable_version, comments, post_proc, technology, ex_use, boundary_used,
                 buildable_acres, boundary_acres, turbine_manufacturer, turbine_model))

            del cursor

            arcpy.AddMessage("Finished writing Boundary to Project History Table")

        else:
            # append to the history table using field mapping to make sure the fields are correct
            fields = ['projectname',
                      'Project_ID',
                      'Buildable_Version',
                      'Comments',
                      'Post_Processing',
                      'Technology',
                      'External_Use',
                      'Boundary_Used',
                      'Buildable_Acres',
                      'Boundary_Acres']

            boundary_desc = arcpy.Describe(boundary_used)
            boundary_used = boundary_desc.catalogPath

            geometries = arcpy.CopyFeatures_management(boundary_used, arcpy.Geometry())
            boundary_acres = geometries[0].getArea("GEODESIC", "ACRES")

            # Execute MakeTableView and delete existing records & update
            deleteWhere = "ProjectName = '{}' AND Project_ID = {} AND buildable_version = {}".format(project_name, project_id,
                                                                                                     buildable_version)

            with arcpy.da.UpdateCursor(project_history_fs, ["ProjectName", "Project_ID", "buildable_version"],
                                       where_clause=deleteWhere) as uCur:
                for row in uCur:
                    arcpy.AddMessage(f"Found & deleting duplicate row -- {row}")
                    uCur.deleteRow()

            arcpy.AddMessage(f"Acres for boundary is - {boundary_acres}")

            cursor = arcpy.da.InsertCursor(project_history_fs,
                                           fields)

            cursor.insertRow((project_name, project_id, buildable_version, comments, post_proc, technology, ex_use, boundary_used,
                              buildable_acres, boundary_acres))

            del cursor

            arcpy.Delete_management(r'\in_memory\Exploded')
            arcpy.Delete_management(r'\in_memory\Dissolved')
            arcpy.Delete_management(r"\in_memory\Buffer")
            arcpy.AddMessage("Finished writing Boundary to Project History Table")

except arcpy.ExecuteError:
    print('-1-')
    print(arcpy.GetMessages(1))
    print('-2-')
    print(arcpy.GetMessages(2))
    arcpy.AddError('-1-')
    arcpy.AddError(arcpy.GetMessages(1))
    arcpy.AddError('-2-')
    arcpy.AddError(arcpy.GetMessages(2))

except Exception as e:
    print(e)
    arcpy.AddError(e)

finally:
    arcpy.Delete_management(r'\in_memory\Exploded')
    arcpy.Delete_management(r'\in_memory\Dissolved')
    arcpy.Delete_management(r"\in_memory\Buffer")

# Checking geometry for the buildables in SDE now that a new record has been added
# field_names = [f.name for f in arcpy.ListFields(buildables_sde) if f.type == 'String']
#
# row_check = []
# for row in arcpy.da.SearchCursor(buildables_sde, field_names):
#     row_check.append(row)
#
# arcpy.AddMessage("Running the check geometry tool on {} feature classes".format(
#     len(row_check)))

# create the layer with only inserted feature
# arcpy.MakeFeatureLayer_management(buildables_feature, "insertlayer",
#                                   where_clause="ProjectName = '{}' AND Project_ID = {} AND buildable_version = {}".format(
#                                       project_name, project_id,
#                                       buildable_version))
# arcpy.CheckGeometry_management("insertlayer", out_table)
#
# # If the geometry had any issues, the results of the error will be written
# geom_error_count = arcpy.GetCount_management(out_table)[0]
#
# if int(geom_error_count) > 0:
#     arcpy.AddMessage("{} geometry problems found, see {} for details.".format(
#         geom_error_count, out_table))
#
#     arcpy.RepairGeometry_management("insertlayer")
#
# arcpy.AddMessage(f"Successfully updated project -- {project_name}")
