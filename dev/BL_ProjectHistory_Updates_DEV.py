import arcpy

def select_buildable_feature(wind_or_solar):
    if wind_or_solar == 'Solar/Other':
        return r"G:\General\GIS\SDEConnections\SDHQRAGDBPRD01_webmap.sde\DBO.Buildable_Lands_Solar_WGS84"
    elif wind_or_solar == 'Wind':
        return r"G:\General\GIS\SDEConnections\SDHQRAGDBPRD01_webmap.sde\DBO.Buildable_Lands_Wind_WGS84"
    else:
        return None

def repair_and_dissolve_geometry(input_polygon):
    arcpy.MultipartToSinglepart_management(input_polygon, r"in_memory\Exploded")
    arcpy.RepairGeometry_management(r"in_memory\Exploded", validation_method='OGC')
    arcpy.Dissolve_management(r"in_memory\Exploded", r"in_memory\Dissolved", "", "", "MULTI_PART")
    arcpy.RepairGeometry_management(r"in_memory\Dissolved", validation_method='OGC')
    return r"in_memory\Dissolved"

def geometry_buffer(dissolved_geometry):
    arcpy.Buffer_analysis(dissolved_geometry, r"in_memory\Buffer", "1 Feet", "FULL")
    arcpy.RepairGeometry_management(r"in_memory\Buffer", validation_method='OGC')
    return r"in_memory\Buffer"

def delete_sde_layer(buildable_feature, project_name, project_id, buildable_version):
    arcpy.AddMessage("Writing buildable to SDE")
    deleteWhere = f"ProjectName = '{project_name}' AND Project_ID = {project_id} AND buildable_version = {buildable_version}"

    with arcpy.da.UpdateCursor(buildable_feature, ["ProjectName", "Project_ID", "buildable_version"],
                               where_clause=deleteWhere) as uCur:
        for row in uCur:
            arcpy.AddMessage(f"Found & deleting duplicate row -- {row}")
            uCur.deleteRow()

def insert_sde_layer(buffer_geometry, buildable_feature, sde_fields,
                     project_name, project_id, buildable_version, comments, buildable_acres, factory_code):
    with arcpy.da.InsertCursor(buildable_feature, sde_fields) as cursor:
        with arcpy.da.SearchCursor(buffer_geometry, "SHAPE@") as search_cursor:
            for row in search_cursor:
                geom_shape = row[0]
        # Set values for fields
        row_values = (project_id, project_name, buildable_version, comments, buildable_acres, factory_code, geom_shape)
        cursor.insertRow(row_values)

def update_sde_layer(buildable_feature, project_name, project_id, buildable_version, comments,
                     buildable_acres, buffer_geometry):
    sde_fields = ['Project_ID', 'ProjectName', 'Buildable_Version', 'Notes', 'Area_ac', 'Coordinate_System', 'SHAPE@']
    delete_sde_layer(buildable_feature, project_name, project_id, buildable_version)
    spatial_ref = arcpy.Describe(buffer_geometry).spatialReference
    factory_code = str(spatial_ref.factoryCode)
    insert_sde_layer(buffer_geometry, buildable_feature, sde_fields,
                     project_name, project_id, buildable_version, comments, buildable_acres, factory_code)


def update_project_history_func(project_name, project_id, buildable_version, wind_or_solar, boundary_used,
                           boundary_used_path, buildable_acres, comments, post_proc, technology, ex_use, project_history_fs):
    arcpy.AddMessage(f"Updating Project History for {project_name}, {project_id}, {buildable_version}")

    # Initialize turbine manufacturer and model variables
    turbine_manufacturer, turbine_model = None, None

    # Handle wind projects
    if wind_or_solar == 'Wind':
        # Extract Turbine Manufacturer and Model
        turbine_manufacturer, turbine_model = get_wind_turbine_info(project_name, project_id, buildable_version)

    # Prepare common fields for project history
    fields, boundary_acres = prepare_project_history_fields(boundary_used_path)

    # Additional fields for wind projects
    if wind_or_solar == 'Wind':
        fields.extend(['Turbine_Manufacturer', 'Turbine_Model'])

    # Delete existing records in project history
    delete_existing_project_history(project_history_fs, project_name, project_id, buildable_version)

    # Add message about the acres of boundary
    arcpy.AddMessage(f"Acres for boundary is - {boundary_acres}")

    # Insert new record into project history
    insert_into_project_history(project_history_fs, fields, project_name, project_id, buildable_version,
                                comments, post_proc, technology, ex_use, boundary_used, buildable_acres,
                                boundary_acres, turbine_manufacturer, turbine_model)

    arcpy.AddMessage("Finished writing Boundary to Project History Table")

def get_wind_turbine_info(project_name, project_id, buildable_version):
    wind_search_where = f"ProjectName = '{project_name}' AND Project_ID = {project_id} AND Buildable_Version = {buildable_version}"
    turbine_manufacturer, turbine_model = None, None

    # Assuming this is the correct URL for the Wind Constraint Tracker FeatureServer
    windDB_info_cursor_url = 'https://geoportal.edf-re.com/rag/rest/services/Other/Wind_Constraint_Tracker/FeatureServer/4'

    with arcpy.da.SearchCursor(windDB_info_cursor_url, ['Turbine_Manufacturer', 'Turbine_Alias'],
                               where_clause=wind_search_where) as cursor:
        for row in cursor:
            if row[0] is not None and row[1] is not None:
                turbine_manufacturer = row[0]
                turbine_model = row[1]

    return turbine_manufacturer, turbine_model

def prepare_project_history_fields(boundary_used_path):
    geometries = arcpy.CopyFeatures_management(boundary_used_path, arcpy.Geometry())
    boundary_acres = geometries[0].getArea("GEODESIC", "ACRES")

    fields = ['projectname', 'Project_ID', 'Buildable_Version', 'Comments', 'Post_Processing', 'Technology',
              'External_Use', 'Boundary_Used', 'Buildable_Acres', 'Boundary_Acres']

    return fields, boundary_acres

def delete_existing_project_history(project_history_fs, project_name, project_id, buildable_version):
    deleteWhere = f"ProjectName = '{project_name}' AND Project_ID = {project_id} AND buildable_version = {buildable_version}"

    with arcpy.da.UpdateCursor(project_history_fs, ["ProjectName", "Project_ID", "buildable_version"],
                               where_clause=deleteWhere) as uCur:
        for row in uCur:
            arcpy.AddMessage(f"Found & deleting duplicate row -- {row}")
            uCur.deleteRow()

def insert_into_project_history(project_history_fs, fields, project_name, project_id, buildable_version,
                                comments, post_proc, technology, ex_use, boundary_used, buildable_acres,
                                boundary_acres, turbine_manufacturer, turbine_model):
    with arcpy.da.InsertCursor(project_history_fs, fields) as cursor:
        if turbine_manufacturer is not None and turbine_model is not None:
            cursor.insertRow((project_name, project_id, buildable_version, comments, post_proc, technology,
                              ex_use, boundary_used, buildable_acres, boundary_acres, turbine_manufacturer,
                              turbine_model))
        else:
            cursor.insertRow((project_name, project_id, buildable_version, comments, post_proc, technology,
                              ex_use, boundary_used, buildable_acres, boundary_acres))

def main():
    buildable_polygon = arcpy.GetParameterAsText(0)
    project_name = arcpy.GetParameterAsText(1)
    project_id = arcpy.GetParameterAsText(2)
    buildable_version = arcpy.GetParameterAsText(3)
    buildable_acres = arcpy.GetParameterAsText(4)

    wind_or_solar = arcpy.GetParameterAsText(5)
    update_project_history = arcpy.GetParameter(6)

    boundary_used = arcpy.GetParameterAsText(7)
    post_proc = arcpy.GetParameterAsText(8)
    comments = arcpy.GetParameterAsText(9)
    technology = arcpy.GetParameterAsText(10)
    ex_use = arcpy.GetParameterAsText(11)

    # debugging
    # buildable_polygon = r'G:\Projects\USA_West\Bonanza\05_GIS\053_Data\BL_v38_09132022.shp'
    # project_name = 'Bonanza'
    # project_id = '3000'
    # buildable_version = '38'
    # buildable_acres = '1793.39'
    #
    # wind_or_solar = 'Solar/Other'
    # update_project_history = True
    #
    # boundary_used = r"G:\Projects\USA_West\Bonanza\05_GIS\053_Data\Boundary_Bonanza_SF299_20200723.shp"
    # post_proc = None
    # comments = 'Updating Boundary.'
    # technology = 'Solar'
    # ex_use = None

    arcpy.env.workspace = r"G:\General\GIS\SDEConnections\SDHQRAGDBPRD01_webmap.sde"
    project_history_fs = r"https://geoportal.edf-re.com/rag/rest/services/Other/Project_History/FeatureServer/2"

    boundary_desc = arcpy.Describe(boundary_used)
    boundary_catalog_path = boundary_desc.catalogPath

    arcpy.AddMessage(f"Using {wind_or_solar} buildable feature")
    buildable_feature = select_buildable_feature(wind_or_solar)
    arcpy.AddMessage("Repairing, dissolving and buffering input polygon")
    dissolved_geometry = repair_and_dissolve_geometry(buildable_polygon)
    buffer_geometry = geometry_buffer(dissolved_geometry)

    arcpy.AddMessage("Updating SDE Layer")
    update_sde_layer(buildable_feature, project_name, project_id, buildable_version, comments, buildable_acres, buffer_geometry)

    if update_project_history:
        arcpy.AddMessage("Updating Project History layer")
        update_project_history_func(project_name, project_id, buildable_version, wind_or_solar,
                               boundary_used, boundary_catalog_path, buildable_acres, comments, post_proc, technology, ex_use,
                               project_history_fs)

if __name__ == '__main__':
    main()